import React, { useEffect, useState } from "react";

function ConferenceForm() {
  //   const [location, setLocation] = useState([]);

  //   const fetchData = async () => {
  //     const url = "http://localhost:8000/api/location/";

  //     const response = await fetch(url);

  //     if (response.ok) {
  //       const data = await response.json();
  //       setlocation(data.location);
  //     }
  //   };

  //   useEffect(() => {
  //     fetchData();
  //   }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Starts"
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
              />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Ends"
                required
                type="date"
                name="ends"
                id="ends"
                className="form-control"
              />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                className="form-control"
                id="description"
                rows="3"
              ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Max presentations"
                required
                type="number"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
              />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Max attendees"
                required
                type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
              />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select
                required
                name="location"
                id="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {/* {location.map((location) => {
                  return (
                    <option key={location.name} value={location.name}></option>
                  );
                })} */}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
